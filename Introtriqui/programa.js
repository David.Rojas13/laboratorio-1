var turno="X";
var jugadas=0;
//Punto de entrada
function iniciarjuego()
{
  // Crear  botones

  let miboton;
  let formulario;
  let i;
  let salto;
  //Crear un objeto que referencie al formulario
  formulario=document.getElementById("formTablero");

  for(i=1;i<10;i++)
  {
    //Cree un objeto input,
   miboton=document.createElement("input");

   //Agregar algunos atributos al objeto
  
   miboton.setAttribute("type","button");
   miboton.setAttribute("id","boton"+i);
   miboton.setAttribute("class","boton");
   miboton.setAttribute("value"," ");
   miboton.setAttribute("onclick","realizarjugada(this.id)");
   formulario.appendChild(miboton);
   if(i%3==0)
   {
    salto=document.createElement("br");
    formulario.appendChild(salto);
   }
  } 
}

function realizarjugada(elemento)
{
  if(turno==="X")
   {
     turno="O";
   }
   else
   {
    turno="X";
   } 
}

function casillaocupada(idboton)
{
   let casilla=document.getElementById(idboton);

  if(casilla.value=="X" || casilla.value=="O")
  {
   alert("La casilla que selecciono esta ocupada. Seleccione otra");
   return false;
  }
  casilla.value=turno;
  return true;
}

function decisionTriqui()
{
  let i;
  let cas1;
  let cas2;
  let cas3;

  //decision triqui filas
  for(i=0;i<9;i=i+3)
  {
    cas1=document.getElementById("boton"+(i+1)).value;
    cas2=document.getElementById("boton"+(i+2)).value;
    cas3=document.getElementById("boton"+(i+3)).value;
    if(cas1===cas2 && cas1===cas3 && cas1!=" ")
    {
      return true
    }
  }
 
  //decision triqui columnas
  for(i=0;i<4;i++)
  {
    cas1=document.getElementById("boton"+ i).value;
    cas2=document.getElementById("boton"+(i+3)).value;
    cas3=document.getElementById("boton"+(i+6)).value;
    if(cas1===cas2 && cas1===cas3 && cas1!=" ")
    {
      return true
    }
  }

  //decision triqui en las diagonales
  let val=[2,4];
  for(i=0;i<2;i++)
  {
    cas1=document.getElementById("boton"+ 5).value;
    cas2=document.getElementById("boton"+(5+val[i])).value;
    cas3=document.getElementById("boton"+(5-val[i])).value;
    if(cas1===cas2 && cas1===cas3 && cas1!=" ")
    {
      return true;
    }
  }
  return false;
}

function limpiartablero()
{
  jugadas=0;
  turno="x";
  for(i=1;i<10;i++)
  {
    boton=document.getElementById("boton"+i)
    boton.value=" ";
  }
}


function decisionjuego(idboton)
{
  let decision,resultado;
  decision=casillaocupada(idboton);
  if(decision==true)
  {
    jugadas++;
    resultado=decisionTriqui();
    if(resultado)
    {
      alert(+turno+" Has sido el ganador!!!");
      limpiartablero();
    }
    else if(jugadas==9)
    {
      alert("Ha ocurrido un empate!!!");
      limpiartablero();
    }
    else
    {
      realizarjugada();
    }
  }
  return false;
}


